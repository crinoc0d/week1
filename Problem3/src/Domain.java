public class Domain {
    String name;
    Owner owner;
    String[] hosts;

    void setupDomain(String name, Owner owner, String[] hosts) {
        this.name = name;
        this.owner = owner;
        this.hosts = hosts;
        System.out.println("The domain was setup with the details from customer.");
    }
}
