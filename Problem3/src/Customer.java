public class Customer {
    void contactReseller(String name, Owner owner, String[] hosts, Domain newDomain) {
        Reseller reseller = new Reseller();
        reseller.contactRegistrar(name, owner, hosts, newDomain);
    }

    void buyDomain(String name, Owner owner, String[] hosts, Domain newDomain) {
        contactReseller(name, owner, hosts, newDomain);
        System.out.println("The customer has bought the domain.");
    }

    public static void main(String[] args) {
        Customer customer = new Customer();

        String domainName = "greenhousefly.com";
        Owner owner = new Owner("David", "Jones", "New York");
        String[] hosts = new String[4];
        hosts[0] = "www";       // www.greenhousefly.com
        hosts[1] = "products";  // products.greenhousefly.com
        hosts[2] = "onsale";    // onsale.greenhousefly.com
        hosts[3] = "support";   // support.greenhousefly.com

        Domain newDomain = null;
        customer.buyDomain(domainName, owner, hosts, newDomain);
    }
}
