public class Owner {
    String firstName;
    String lastName;
    String address;

    Owner(String firstName, String lastName, String address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
    }
}
