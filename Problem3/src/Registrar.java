public class Registrar {
    void createDomainProcess() {
        System.out.println("The registrar created the domain process.");
    }

    void contactRegistry(String name, Owner owner, String[] hosts, Domain newDomain) {
        Registry registry = new Registry();

        System.out.println("The registrar has contacted the registry.");
        registry.createDomain(name, owner, hosts, newDomain);
    }
}
