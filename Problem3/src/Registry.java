public class Registry {
    void createDomain(String name, Owner owner, String[] hosts, Domain newDomain) {
        newDomain = new Domain();

        System.out.println("The registry has created the domain.");
        newDomain.setupDomain(name, owner, hosts);
    }
}
