
public class ConnectionManager {
	Connection[] listConnections;
	static final int MAX_NUMBER_CONNECTIONS = 10;
	int sizeList = 0;
	
	ConnectionManager() {
		listConnections = new Connection[MAX_NUMBER_CONNECTIONS];
	}
	
	String createConnection(String username, String password) {
		  if (sizeList < MAX_NUMBER_CONNECTIONS) {
			  listConnections[sizeList] = new Connection();
			  listConnections[sizeList].username = username;
			  listConnections[sizeList].password = password;
			  sizeList++;
			  
			  return "Connection created for " + username + ".";
		  } else {
			  return null;
		  }
	}
	
	void closeConnection(String username, String password) {
		boolean isFound = false;
		
		if (listConnections[sizeList - 1].username.equals(username)) {
			isFound = true;
		} else {
			for (int i = 0; i < sizeList - 1; i++) {
				if (listConnections[i].username.equals(username)) {
					for (int j = i; j < sizeList - 1; j++) {
						listConnections[j] = listConnections[j + 1];
					}
					isFound = true;
					break;
				}
			}
		}
		
		if (isFound) {
			listConnections[sizeList - 1] = null;
			sizeList--;
			System.out.println("Connection " + username + " was closed.");
		} else {
			System.out.println("Connection " + username + " cannot be closed. The connection is not active.");
		}
		
	}
	
	void listConnections() {
		
		System.out.println();
		System.out.println("Open connections (" + sizeList + "):");
		for (int i = 0; i < sizeList; i++) {
			System.out.println(listConnections[i].username);
		}
		System.out.println();
	}
	
	private class Connection {
		String username;
		String password;
	}
	
	
}
