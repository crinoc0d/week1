public class ClientProgrammer {

	static void tryConnection(ConnectionManager mngConnections, String username, String password) {
		
		String connection = mngConnections.createConnection(username, password);
		
		if (connection != null) {
			System.out.println(connection);
		} else {
			System.out.println("No more connections are possible. Wait until a connection is freed up.");
		}
	}
	
	public static void main(String[] args) {
		
		ConnectionManager mngConnections = new ConnectionManager();		
		
		tryConnection(mngConnections, "romaal1", "abcdef");
		tryConnection(mngConnections, "dumian1", "password321");
		tryConnection(mngConnections, "dumian2", "password123");
		tryConnection(mngConnections, "popere1", "password!");
		tryConnection(mngConnections, "popere2", "password123!");
		mngConnections.closeConnection("dumian1", "password123");
		mngConnections.listConnections();
		
		tryConnection(mngConnections, "stanla1", "password024");
		tryConnection(mngConnections, "raduan1", "psw123");
		mngConnections.closeConnection("raduan1", "password123");
		mngConnections.listConnections();
		
		tryConnection(mngConnections, "butacr1", "client123");
		tryConnection(mngConnections, "butacr2", "client321");
		tryConnection(mngConnections, "butoio1", "password123");
		tryConnection(mngConnections, "butacr3", "password123");
		tryConnection(mngConnections, "badima1", "psw456");
		tryConnection(mngConnections, "branti1", "psw456!");
		mngConnections.listConnections();
		
		mngConnections.closeConnection("butoio1", "password123");
		tryConnection(mngConnections, "branti1", "psw456!");
		mngConnections.listConnections();
	}
}
