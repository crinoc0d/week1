import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.io.*;
import java.util.ArrayList;

public class Personality {
    String firstName;
    String lastName;
    Calendar dateOfBirth;
    Calendar dateOfDeath;

    static void storeInformation(ArrayList<Personality> listPersonalities) {
        String csvFile = "./src/ListPersonalities.csv";
        String line = "";
        String csvSplitBy = ",";
        Personality newPersonality;

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

            while ((line = br.readLine()) != null) {
                // Use comma as separator
                String[] dataMembers = line.split(csvSplitBy);
                newPersonality = new Personality();
                newPersonality.firstName = dataMembers[0];
                newPersonality.lastName = dataMembers[1];

                // Date format: month/day/year
                String[] dateFormat = dataMembers[2].split("\\/");
                newPersonality.dateOfBirth = Calendar.getInstance();
                newPersonality.dateOfBirth.set(Integer.parseInt(dateFormat[2]),         // year
                        Integer.parseInt(dateFormat[0]) - 1,                    // month
                        Integer.parseInt(dateFormat[1]));                               // day

                // Consider personalities that are alive (dod = null)
                if (dataMembers.length == 4) {
                    dateFormat = dataMembers[3].split("\\/");
                    newPersonality.dateOfDeath = Calendar.getInstance();
                    newPersonality.dateOfDeath.set(Integer.parseInt(dateFormat[2]),     // year
                            Integer.parseInt(dateFormat[0]) - 1,                // month
                            Integer.parseInt(dateFormat[1]));                           // day
                }
                else {
                    newPersonality.dateOfDeath = Calendar.getInstance();
                }

                if (!findDuplicatedPersonality(newPersonality, listPersonalities)) {
                    listPersonalities.add(newPersonality);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static boolean findDuplicatedPersonality(Personality personalityToFind, ArrayList<Personality> listPersonalities) {
        for (Personality personality: listPersonalities) {
            if (personality.firstName.equals(personalityToFind.firstName)
                && personality.lastName.equals(personalityToFind.lastName)) {
                return true;
            }
        }
        return false;
    }

    static void printInformation(ArrayList<Personality> listPersonalities) {
        SimpleDateFormat ft = new SimpleDateFormat ("MMM dd, YYYY");

        for (Personality personality: listPersonalities) {
            System.out.println(personality.firstName + " "
                    + personality.lastName + " ("
                    + ft.format(personality.dateOfBirth.getTime()) + " - "
                    + ft.format(personality.dateOfDeath.getTime()) + ")");
        }
    }

    public static void main(String[] args) {
        ArrayList<Personality> listPersonalities = new ArrayList();

        storeInformation(listPersonalities);
        printInformation(listPersonalities);
    }
}
