import java.util.Arrays;

public class PrimeNumbers {

    static void findPrimeNumbers(boolean[] primeNumbers) {
        Arrays.fill(primeNumbers, true);
        primeNumbers[0] = primeNumbers[1] = false;

        for (int i = 2; i < primeNumbers.length; i++) {
            if (primeNumbers[i]) {
                for (int j = 2; i * j < primeNumbers.length; j++) {
                    primeNumbers[i * j] = false;
                }
            }
        }
    }

    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        boolean[] primeNumbers = new boolean[n + 1];

        findPrimeNumbers(primeNumbers);
        for (int i = 1; i < n; i++) {
            if (primeNumbers[i]) {
                System.out.print(i + "-PRIME, ");
            } else {
                System.out.print(i + ", ");
            }
        }

        if (primeNumbers[n]) {
            System.out.println(n + "-PRIME");
        }
        else {
            System.out.println(n);
        }
    }
}
